#include<stdio.h>
int count=0;
void toh(int,char,char,char);

int main()
{
	int n;
	scanf("%d",&n);
	toh(n,'A','B','C');
	printf("No. of steps=%d\n",count);
	return 0;
}

void toh(int n,char sou,char dest,char aux)
{
	if(n==1)
	{
		printf("move disk %d from %c to %c\n",n,sou,dest);
		count++;
		return;
	}
	toh(n-1,sou,aux,dest);
	printf("move disk %d from %c to %c\n",n,sou,dest);
	toh(n-1,aux,dest,sou);
	count++;
}

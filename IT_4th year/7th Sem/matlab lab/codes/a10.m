clc;
a = [1,-2,4;-1,4,2;2,2,6];
visitedcol = [0,0,0];
visitedrow = [0;0;0];
i=1;
j=1;
while(i<=size(a,1) && size(a,1)>2)
    if visitedrow(i)==1
        i=i+1;
        continue;
    end
    visitedrow(i)=1;
    j=1;
    while(j<=size(a,1))
        if i==j
            j=j+1;
            continue;
        end
        flag=0;
        for k=1:size(a,2)
            if a(i,k)>a(j,k)
                flag=1;
                break;
            end
        end
        if flag==0
            a(i,:)=[];
            visitedrow(i,:)=[];
            break;
        end
        j=j+1;
    end
    if flag==1
        i=i+1;
    end
end
i=size(a,2);
j=1;
while(i>=0 && size(a,2)>2)
    if visitedcol(i)==1
        i=i-1;
        continue;
    end
    visitedcol(i)=1;
    j=1;
    while(j<=size(a,2))
        if i==j
            j=j+1;
            continue;
        end
        flag=0;
        for k=1:size(a,1)
            if a(k,i)<a(k,j)
                flag=1;
                break;
            end
        end
        if flag==0
            a(:,i)=[];
            visitedcol(:,i)=[];
            break;
        end
        j=j+1;
    end
    if flag==1
        i=i-1;
    end
end
disp(a);
p1=(a(2,2)-a(2,1))/(a(2,2)-a(2,1)+a(1,1)-a(1,2));
disp('p1');
disp(p1);
disp('p2');
disp(1-p1);
q1=(a(2,2)-a(1,2))/(a(2,2)-a(2,1)+a(1,1)-a(1,2));
disp('q1');
disp(q1);
disp('q2');
disp(1-q1);
y=(a(2,2)*a(1,1)-a(2,1)*a(1,2))/(a(2,2)-a(2,1)+a(1,1)-a(1,2));
disp('gamma');
disp(y);
Socket is an interface between an application process and transport layer. The application process can send/receive messages to/from another application via a socket.

A socket program has two parts:
1) Client part
2) Server part

To communicate applications between different hosts TCP connections need to be established. One computer operates as client which always requests for a TCP connection to be established with another computer known as server. 

There are a few steps to set up socket connection.

For the server:
1. socket()
2. bind()
3. listen()
4. accept()
5. recv/send()
6. close()

For the client:
1. socket()
2. connect()
3. recv()/send()
4. close()

Different types of socket programs:

1. Echo Program - In an echo program the client sends a message to the server and the server returns(echoes) the same message to the client.

2. Client-Server Chat Program - In a Client-Server Chat Program, the client sends a message to the server and the server replies back to the client. This can be done asynchronously too using threads where anyone can send a message independent of the other.

3. Peer to Peer Program - In a Peer to Peer Program, no one is predefined as the client or the server. Anyone can be the client and anyone can be the server and there is only one program for both of them unlike client-server chat. This can also be done asynchronously using threads where anyone can send a message independent of the other.
#include<bits/stdc++.h>
using namespace std;
class Complex
{
private:
    int a,b;
public:
    Complex(int i, int j)
    {
        a=i,b=j;
    }
    Complex operator+(Complex i)
    {
        return Complex(a+i.a,b+i.b);
    }
    Complex operator-(Complex i)
    {
        return Complex(a-i.a,b-i.b);
    }
    Complex operator*(Complex i)
    {
        return Complex(a*(i.a)-b*(i.b),a*(i.b)+b*(i.a));
    }
    void display()
    {
        if(b<0) cout<< a << b << "i\n";
        else cout<< a << "+" << b << "i\n";
    }
};


int main()
{
    Complex p(1,2),q(3,4), r=p+q, s=p-q, t=p*q;
    r.display(),s.display(),t.display();
    return 0;
}
